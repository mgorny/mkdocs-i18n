# Translated title

This page have a title in nav configuration that is translated using i18n plugin configuration.

See also <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L29> and <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L53>
