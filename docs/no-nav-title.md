# No title in nav

This page is in english, catalan and spanish. And no title configured in nav configuration. This is processed with default language, due to no language is specified in the markdown filename.

See also <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/mkdocs.yml#L40> and <https://gitlab.com/mkdocs-i18n/mkdocs-i18n/-/blob/main/docs/no-nav-title.md>
